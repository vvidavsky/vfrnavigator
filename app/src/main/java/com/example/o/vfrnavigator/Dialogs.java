package com.example.o.vfrnavigator;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import java.util.Arrays;

import Database.DBHandler;
import Globals.Constants;

/**
 * Created by vlad on 25/08/2017.
 */

public class Dialogs implements View.OnClickListener {
    private Context cont;
    private Dialog dialog;
    private DBHandler dbh;
    private UIOperator uic;
    private String rowId;

    Dialogs(Activity activity) {
        cont = activity;
        dbh = new DBHandler(activity);
        uic = new UIOperator(activity);
        dialog = new Dialog(activity);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
    }

    /**
     * Вот эта хуета открывает окно апликативных настроек.
     */
    void settingsDialog() {
        dialog.setContentView(R.layout.dialog_settings);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

        final Button closeDialog = dialog.findViewById(R.id.close);
        final Button saveSettings = dialog.findViewById(R.id.save_settings);
        final Button createFlightPlan = dialog.findViewById(R.id.create_flight_plan);

        final EditText reg_num = dialog.findViewById(R.id.plane_registration);
        final EditText plane_model = dialog.findViewById(R.id.plane_model);
        final Spinner plane_type = dialog.findViewById(R.id.plane_type_picker);

        final String[] aircraftTypes = cont.getResources().getStringArray(R.array.aircraft_types);
        rowId = dbh.getSettingsItemAtIndex(0, false);
        reg_num.setText(dbh.getSettingsItemAtIndex(1, false));
        plane_model.setText(dbh.getSettingsItemAtIndex(2, false));
        plane_type.setSelection(Integer.parseInt(dbh.getSettingsItemAtIndex(3, true)));


        closeDialog.setOnClickListener(this);
        createFlightPlan.setOnClickListener(this);
        saveSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String rn = reg_num.getText().toString();
                String pm = plane_model.getText().toString();
                String ap = plane_type.getSelectedItem().toString();
                int index = Arrays.asList(aircraftTypes).indexOf(ap);
                dbh.updateApplicationSettings(rowId, rn, pm, index);
                dialog.dismiss();
            }
        });
        dialog.show();

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.close:
                dialog.dismiss();
                break;
            case R.id.create_flight_plan:
                uic.activateCreatelightPlanMode();
                dialog.dismiss();
                break;
        }
    }
}
