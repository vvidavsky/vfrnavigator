package com.example.o.vfrnavigator;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.util.SparseArray;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import Constructors.WayPoint;
import Database.DBHandler;
import Globals.Constants;

/**
 * Created by vlad on 21/08/2017.
 */

public class UIOperator implements View.OnClickListener {
    private DBHandler dbh;
    private Context context;
    private SparseArray<Marker> sparseMarker = new SparseArray<>();
    private static List<WayPoint> tempWaypoints = new ArrayList<>();
    private static List<Polyline> lines = new ArrayList<>();
    private LinearLayout editPanel;
    private EditText flightPlanName;


    public UIOperator(Activity activity) {
        context = activity;
        dbh = new DBHandler(activity);
        editPanel = activity.findViewById(R.id.edit_path_panel);
        flightPlanName = activity.findViewById(R.id.flight_plan_name);
        Button savePlane = activity.findViewById(R.id.save_flight_plan);
        Button cancelPlan = activity.findViewById(R.id.cancel_flight_plan);
        savePlane.setOnClickListener(this);
        cancelPlan.setOnClickListener(this);
    }

    /**
     * Данный метод показывает панель управления режима Создания/Изменения
     * плана полета.
     */
    void activateCreatelightPlanMode(){
        MainActivity.createMode = true;
        editPanel.setVisibility(View.VISIBLE);
    }

    /**
     * Данный метод закрывает панель управления режима Создания/Изменения
     * плана полета.
     */
    private void deactivateCreateFlightPlanMode(Boolean save){
        MainActivity.createMode = false;
        editPanel.setVisibility(View.GONE);
        if(save){
            String planName = flightPlanName.getText().toString();
            String id = UUID.randomUUID().toString();
            dbh.saveFlightPlan(planName, id, tempWaypoints.toString());
        }
    }

    /**
     * Данная поеботина ответственна за то чтобы нарисовать главный маркер т.е.
     * самолетик вокруг которого вертится вся наша планета, на карте в правильной
     * позиции.
     *
     * @param lat - double Широта
     * @param lng - double Долгота
     */
    public void AddUpdateMainMapMarker(double lat, double lng, float bearing) {
        String planeIcon = "plane_" + dbh.getSettingsItemAtIndex(3, true);
        removeMainMapMarker();
        LatLng planeLocation = new LatLng(lat, lng);
        Marker marker = MainActivity.mMap.addMarker(new MarkerOptions()
                .icon(BitmapDescriptorFactory.fromBitmap(resizeMapIcons(planeIcon, 120, 120)))
                .anchor(0.5f, 0.5f)
                .position(planeLocation)
                .rotation(bearing)
                .flat(true)
                .title("Marker in Sydney"));
        sparseMarker.put(Constants.PLANE_MARKER_ID, marker);
        if (!MainActivity.createMode) {
            MainActivity.mMap.moveCamera(CameraUpdateFactory.newLatLng(planeLocation));
        }
    }

    /**
     * Данный метод изменяет величину маркера на карте
     *
     * @param iconName - String имя картинки маркера
     * @param width    - желаемая ширина
     * @param height   - желаемая высота
     * @return - Объект с параметрами маркера
     */
    private Bitmap resizeMapIcons(String iconName, int width, int height) {
        Bitmap imageBitmap = BitmapFactory.decodeResource(context.getResources(), context.getResources().getIdentifier(iconName, "drawable", context.getPackageName()));
        return Bitmap.createScaledBitmap(imageBitmap, width, height, false);
    }

    /**
     * Данный метод стерает нахуй главный маркер т.е. летящий самолетик
     * с карты. Данный метод запрашивается перед каждым изменением геопозиции
     * самолетика ибо если его не стирать, то он будет размножен при каждом
     * изменении позиции, а нахуй нам это надо.
     */
    private void removeMainMapMarker() {
        Marker marker = sparseMarker.get(Constants.PLANE_MARKER_ID);
        if (marker != null) {
            marker.remove();
            sparseMarker.remove(Constants.PLANE_MARKER_ID);
        }
    }

    /**
     * Данный метод вырисовывает на карте навигационные точки пути,
     * чтобы летчик знал куда хуярить и не заблудился
     * нахуй нигде.
     *
     * @param position - Объект с широтой и долготой для позиционирования нав. точки
     */
    void addWaypointToMap(LatLng position) {
        UUID uuid = UUID.randomUUID();
        tempWaypoints.add(new WayPoint(uuid.toString(), Constants.MARKER_WAYPOINT, (float) position.latitude, (float) position.longitude));
        MainActivity.mMap.addMarker(new MarkerOptions()
                .icon(BitmapDescriptorFactory.fromBitmap(resizeMapIcons("circle", 120, 120)))
                .anchor(0.5f, 0.5f)
                .position(position)
                .flat(true)
                .snippet(uuid.toString())
                .title(""))
                .setDraggable(true);

        if (tempWaypoints.size() > 1) {
            WayPoint from = tempWaypoints.get(tempWaypoints.size() - 2);
            WayPoint to = tempWaypoints.get(tempWaypoints.size() - 1);
            Float fromLat = from.getLat();
            Float fromLng = from.getLng();
            Float toLat = to.getLat();
            Float toLng = to.getLng();
            connectWaypointsWithLines(fromLat, fromLng, toLat, toLng);
        }
    }

    /**
     * Данный метод соединяет точки на карте линиями
     *
     * @param fromLat - широта исходной точки
     * @param fromLng - долгота исходной точки
     * @param toLat   - широта точки назначения
     * @param toLng   - долгота точки назначения
     */
    private void connectWaypointsWithLines(Float fromLat, Float fromLng, Float toLat, Float toLng) {
        lines.add(MainActivity.mMap.addPolyline(new PolylineOptions()
                .add(new LatLng(fromLat, fromLng), new LatLng(toLat, toLng))
                .width(Constants.PATH_WIDTH)
                .color(Color.RED)));
    }

    /**
     * Данный метод вызывается каждый раз когда пользователь меняет местоположения
     * навигационного маркера на карте.
     * @param markerId - id маркера который был перемещен
     * @param position - новые координаты маркера на карте
     */
    void onMarkerRelocated(String markerId, LatLng position) {
        for (WayPoint wp : tempWaypoints) {
            if (wp.getId().equals(markerId)) {
                int index = tempWaypoints.indexOf(wp);
                wp.setLat((float) position.latitude);
                wp.setLng((float) position.longitude);
                if (tempWaypoints.size() > 1) {
                    if (index > 0) {
                        WayPoint prev = tempWaypoints.get(index - 1);
                        Polyline beforeWpLine = lines.get(index - 1);
                        if (beforeWpLine != null) {
                            updatePolyline(beforeWpLine, prev, wp);
                        }
                    }
                    if (index + 1 <= tempWaypoints.size() - 1) {
                        WayPoint next = tempWaypoints.get(index + 1);
                        Polyline afterWpLine = lines.get(index);
                        if (next != null) {
                            updatePolyline(afterWpLine, wp, next);
                        }
                    }
                }
            }
        }
    }

    /**
     * Данный метод перерисовывает линиию соединяющую две точки на карте,
     * согласно геолокационным координатам, вызывается после исменения локации
     * точки на карте пользователем.
     * @param line - объект полилайна который нужно апдейтнуть
     * @param fromWp - начальная точка
     * @param toWp - конечная точка
     */
    private void updatePolyline(Polyline line, WayPoint fromWp, WayPoint toWp) {
        List<LatLng> beforeWpLineCoords = line.getPoints();
        LatLng prevStart = new LatLng(fromWp.getLat(), fromWp.getLng());
        LatLng prevEnd = new LatLng(toWp.getLat(), toWp.getLng());
        beforeWpLineCoords.set(0, prevStart);
        beforeWpLineCoords.set(1, prevEnd);
        line.setPoints(beforeWpLineCoords);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.save_flight_plan:
                deactivateCreateFlightPlanMode(true);
                break;
            case R.id.cancel_flight_plan:
                deactivateCreateFlightPlanMode(false);
                break;
        }
    }
}
