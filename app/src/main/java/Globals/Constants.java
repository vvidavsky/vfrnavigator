package Globals;

/**
 * Created by vlad on 22/08/2017.
 */

public class Constants {
    public static final Integer PLANE_MARKER_ID = 161183;
    public static final Integer PATH_WIDTH = 4;
    public static final String MARKER_WAYPOINT = "waypoint";

    public static final String APP_SETTINGS_TABLE = "app_settings";
    public static final String SET_PLANE_REG = "aircraft_reg_num";
    public static final String SET_PLANE_MODEL = "aircraft_model";
    public static final String SET_PLANE_TYPE = "aircraft_type";

    public static final String FLIGHT_PLANS_TABLE = "flight_plans";
    public static final String PLAN_NAME = "plan_name";
    public static final String PLAN_ID = "plan_id";
    public static final String PLAN_PATH = "plan_path";
}
