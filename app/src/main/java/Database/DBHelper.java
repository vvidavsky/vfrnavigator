package Database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;

import Globals.Constants;

/**
 * Created by vlad on 25/08/2017.
 */

class DBHelper extends SQLiteOpenHelper {

    DBHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        String settingsTable = ("CREATE TABLE " + Constants.APP_SETTINGS_TABLE + " (_id INTEGER PRIMARY KEY, " + Constants.SET_PLANE_REG + " TEXT, " + Constants.SET_PLANE_MODEL + " TEXT, " + Constants.SET_PLANE_TYPE + " INTEGER)");
        String flightPlansTable = ("CREATE TABLE " + Constants.FLIGHT_PLANS_TABLE + " (_id INTEGER PRIMARY KEY, " + Constants.PLAN_NAME + " TEXT, " + Constants.PLAN_ID + " TEXT, " + Constants.PLAN_PATH + " TEXT)");
        try {
            sqLiteDatabase.execSQL(settingsTable);
            sqLiteDatabase.execSQL(flightPlansTable);
        } catch (SQLiteException e) {
            e.getCause();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
}
