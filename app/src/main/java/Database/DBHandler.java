package Database;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;

import Globals.Constants;

/**
 * Created by vlad on 25/08/2017.
 */

public class DBHandler {
    private DBHelper dbHelper;

    public DBHandler(Context context) {
        dbHelper = new DBHelper(context, "ClockIO.db", null, 1);
    }

    /**
     * Данный метод проверяет существуют ли данные в таблице настроек
     * либо она пуста.
     *
     * @return Boolean
     */
    public Boolean checkIfSettingsAreEmpty() {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        String count = "SELECT count(*) FROM " + Constants.APP_SETTINGS_TABLE;
        Cursor cursor = db.rawQuery(count, null);
        cursor.moveToFirst();
        int length = cursor.getInt(0);
        cursor.close();
        return length == 0;
    }

    /**
     * Метод создающий объект с данными о настройках приложения
     * исполъзуется только один раз при самом первом запуске приложения
     * для того чтобы создать первоначальные настройки в базе данных
     */
    public void createSettingsTable() {
        SQLiteDatabase database = dbHelper.getWritableDatabase();
        try {
            ContentValues values = new ContentValues();
            values.put(Constants.SET_PLANE_TYPE, 0);
            database.insertOrThrow(Constants.APP_SETTINGS_TABLE, null, values);
        } catch (SQLiteException e) {
            e.getCause();
        } finally {
            if (database.isOpen()) database.close();
        }
    }

    /**
     * Данный метод осуществляет сохранение изменений в панели настроек
     * @param rowId    - ID строки которую нужно обновить.
     * @param regNum   - Регистрационный номер летающего средства
     * @param model - Модель летающего средства
     * @param type - Тип летающего средства
     */
    public void updateApplicationSettings(String rowId, String regNum, String model, int type) {
        SQLiteDatabase database = dbHelper.getWritableDatabase();
        try {
            ContentValues values = new ContentValues();
            values.put(Constants.SET_PLANE_REG, regNum);
            values.put(Constants.SET_PLANE_MODEL, model);
            values.put(Constants.SET_PLANE_TYPE, type);
            database.update(Constants.APP_SETTINGS_TABLE, values, "_id = ?", new String[]{rowId});
        } catch (SQLiteException e) {
            e.getCause();
        } finally {
            if (database.isOpen()) database.close();
        }
    }

    /**
     * Метод возвращающий значение определенной ячейки из базы данных
     *
     * @param index Integer желаемый индех ячейки в базе данных
     * @param returnInt is true if integer value should be picked
     * @return String значение полученное из базы данных
     */
    public String getSettingsItemAtIndex(Integer index, Boolean returnInt) {
        String valueToReturn;
        Cursor cursor = getTableData(Constants.APP_SETTINGS_TABLE);
        try {
            cursor.moveToFirst();
            if(returnInt){
                valueToReturn = String.valueOf(cursor.getInt(index));
            } else {
                valueToReturn = cursor.getString(index);
            }
        } finally {
            cursor.close();
        }
        return valueToReturn;
    }

    /**
     * Метод возвращающий данные указаной даблицы
     * @param tableName - String название таблицы в базе данных из которой нужно вытащить результаты
     */
    public Cursor getTableData(String tableName) {
        Cursor cursor = null;
        SQLiteDatabase database = dbHelper.getReadableDatabase();
        try {
            cursor = database.query(tableName, null, null, null, null, null, null);
        } catch (SQLiteException e) {
            e.getCause();
        }
        return cursor;
    }

    /**
     * Данная функция стерает к хуям все записи из определенной таблицы в базе данных
     * @param tableName имя таблицы которую нужно стереть все к ебеням.
     */
    public void cleanDbTable(String tableName){
        SQLiteDatabase database = dbHelper.getWritableDatabase();
        String deleteQuery = ("DELETE FROM " + tableName);
        try {
            database.execSQL(deleteQuery);
        } catch (SQLiteException e) {
            e.getCause();
        }
    }

    /**
     * Данный метод сохраняет созданный план полета в базе данных
     * чтобы летчик мог использовать его по мере нужды а не пересоздавал
     * его заного как конченый уебан.
     * @param flightPlanName - названия плана полета (понятное для простого смертного)
     * @param flightPlanId - id плана полета
     * @param flightPath - лист с координатами и навигационными точками
     */
    public void saveFlightPlan(String flightPlanName, String flightPlanId, String flightPath) {
        SQLiteDatabase database = dbHelper.getWritableDatabase();
        try{
            ContentValues values = new ContentValues();
            values.put(Constants.PLAN_NAME, flightPlanName);
            values.put(Constants.PLAN_ID, flightPlanId);
            values.put(Constants.PLAN_PATH, flightPath);
            database.insertOrThrow(Constants.FLIGHT_PLANS_TABLE, null, values);
            //tc.showCustomToast(5);
        } catch (SQLiteException e){
            e.getCause();
        } finally {
            if(database.isOpen()) database.close();
        }
    };
}
