package Constructors;

/**
 * Created by vlad on 21/09/2017.
 */

public class WayPoint {
    private String id, type;
    private Float lat, lng;

    public String getType() {
        return type;
    }

    public void setType(String waypoint) {
        this.type = waypoint;
    }
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Float getLat() {
        return lat;
    }

    public void setLat(Float lat) {
        this.lat = lat;
    }

    public Float getLng() {
        return lng;
    }

    public void setLng(Float lng) {
        this.lng = lng;
    }

    public WayPoint(String id, String type, Float lat, Float lng) {
        this.id = id;
        this.type = type;
        this.lat = lat;
        this.lng = lng;
    }
}
